<?php

namespace RedRay\Multilingual\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class Language extends Model
{
    use HasFactory;

    protected $fillable = [
        'is_main',
        'code',
        'name',
    ];

    protected $casts = [
        'is_main' => 'boolean',
    ];

    public function scopeMain($query)
    {
        $query->where('is_main', true);
    }

    public function scopeNotMain($query)
    {
        $query->where('is_main', false);
    }

    public static function mainLanguageId(): ?int
    {
        return self::main()->first()->id;
    }

    public static function firstByCodeOrMain(string $code): self
    {
        $languages = self::get();

        return  $languages->firstWhere('code', $code) ?? $languages->firstWhere('is_main', true);
    }

    public static function tableExists(): bool
    {
        try {
            return Schema::hasTable(self::make()->getTable());
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            Log::error($exception->getTraceAsString());

            return false;
        }
    }
}
