<?php

namespace RedRay\Multilingual;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use RedRay\Multilingual\Middleware\DefineCurrentLanguage;
use RedRay\Multilingual\Models\Language;

class MultilingualProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerPublishables();

        $routes = config('multilingual.multilingual_web_routes');

        $routesPath = base_path($routes);

        if (file_exists($routesPath) && is_file($routesPath)) {
            if (Language::tableExists()) {
                Language::get()->each(function (Language $language) use ($routes) {
                    Route::prefix($language->is_main ? '' : $language->code)
                        ->middleware(['web', DefineCurrentLanguage::class])
                        ->as($language->code.'.')
                        ->group(base_path($routes));
                });
            }
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/multilingual.php', 'multilingual');

        Blueprint::macro('foreignLanguageId', function () {
            $this->unsignedBigInteger('language_id')->index();

            $this
                ->foreign('language_id')
                ->on('languages')
                ->references('id')
                ->onDelete('cascade');
        });

        Blueprint::macro('standardDescriptionAttributes', function () {
            $this->foreignLanguageId();
            $this->string('name');
            $this->text('description')->nullable();
            $this->text('meta_title')->nullable();
            $this->text('meta_description')->nullable();
        });
    }

    protected function registerPublishables()
    {
        $this->publishes([
            __DIR__.'/../config/multilingual.php' => config_path('multilingual.php'),
        ], 'config');

        $this->publishes([__DIR__.'/../database/migrations' => database_path('migrations')], 'migrations');
    }
}
