<?php

namespace RedRay\Multilingual\Database;

use Illuminate\Database\Seeder;
use RedRay\Multilingual\Models\Language;

class LanguageSeeder extends Seeder
{
    public function run()
    {
        Language::create([
            'is_main' => true,
            'code' => 'uk',
            'name' => 'Украинский',
        ]);

        Language::create([
            'is_main' => false,
            'code' => 'ru',
            'name' => 'Русский',
        ]);
    }
}
