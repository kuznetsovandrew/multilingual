<?php

namespace RedRay\Multilingual\Middleware;

use Closure;
use Illuminate\Support\Facades\Lang;
use Jenssegers\Date\Date;
use RedRay\Multilingual\Models\Language;

class DefineCurrentLanguage
{
    public function handle($request, Closure $next)
    {
        if (Language::tableExists()) {
            $currentLanguage = Language::firstByCodeOrMain((string) $request->segment(1));

            config()->set([
                'app.locale' => $currentLanguage->code,
                'multilingual.current_language_id' => $currentLanguage->id,
            ]);

            if (class_exists(Date::class)) {
                Date::setLocale($currentLanguage->code);
            }

            Lang::setLocale($currentLanguage->code);
        }

        return $next($request);
    }
}
