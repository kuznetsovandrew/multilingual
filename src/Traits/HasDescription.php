<?php

namespace RedRay\Multilingual\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Str;

trait HasDescription
{
    public function descriptions(): HasMany
    {
        return $this->hasMany($this->getDescriptionModelClass());
    }

    public function scopeWithDescription(Builder $query, int $languageId)
    {
        /** @var $descriptionModelClass Model * */
        $descriptionModelClass = get_class($this->descriptions()->getModel());

        $subQuery = $this
            ->getDescriptionSubQuery($descriptionModelClass, $languageId)
            ->select($this->descriptions()->getForeignKeyName(), ...$this->getDescriptionModelSelectFields());

        if (is_null($query->getQuery()->columns)) {
            $query->select([$query->getQuery()->from.'.*', ...$this->getDescriptionModelSelectFields()]);
        }

        $query->leftJoinSub($subQuery, 'descriptions', function (JoinClause $join) {
            $join->on(
                'descriptions.'.$this->descriptions()->getForeignKeyName(),
                static::getTableName().'.'.$this->descriptions()->getLocalKeyName()
            );
        });
    }

    public static function getTableName(): string
    {
        return static::make()->getTable();
    }

    protected function getDescriptionSubQuery(string $class, int $languageId)
    {
        return $class::where('language_id', $languageId);
    }

    protected static function getDescriptionModelClass(): string
    {
        return Str::beforeLast(static::class, '\\')
            .'\\'.Str::afterLast(static::class, '\\')
            .'Description';
    }

    public function updateOrCreateDescriptions($descriptionsData)
    {
        foreach ($descriptionsData as $descriptionData) {
            $description = $this->descriptions()->firstOrNew(['language_id' => $descriptionData['language_id']]);

            $description->fill($descriptionData);

            $description->language_id = $descriptionData['language_id'];

            $description->save();
        }
    }

    protected function getDescriptionModelSelectFields(): array
    {
        return [
            'name',
            'description',
            'meta_title',
            'meta_description',
        ];
    }
}
