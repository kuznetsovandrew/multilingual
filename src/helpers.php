<?php

if (! function_exists('localedRoute')) {
    function localedRoute($name, $parameters = [], $absolute = true, $local = null)
    {
        if (! isset($local)) {
            $local = config('app.locale');
        }

        return route("$local.$name", $parameters, $absolute);
    }
}
