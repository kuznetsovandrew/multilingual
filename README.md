# This package allows preventing indexing of a website during development

### Publish assets
    php artisan vendor:publish --provider="RedRay\Multilingual\MultilingualProvider"

### Add languages table
The package includes a migration, a seeder and a model for multilingual.
 
    php artisan migrate
    php artisan db:seed --class="RedRay\Multilingual\Database\LanguageSeeder"

By default, multilingual routes locates in:
 
    routes/multilingual_web.php
    
But you are able to change this file in the config.

### Helpers:

#### Migrations

    Schema::create('product_descriptions', function (Blueprint $table) {
        ...
        $table->standardDescriptionAttributes()
        ...
        // or if you need only foreign language 
        ...
        $table->foreignLanguageId()
        ...
    });

#### Models

Also, there is a convenient trait for multilingual models:

    class Article extens Model {
        use \RedRay\Multilingual\Traits\HasDescription;
    }
    
    class ArticleDescription extens Model {
        protected $fillable = [
            'name',
            'description',
            'meta_title',
            'meta_description',
        ];
    }

Now you are able to make requests like this:

    $languageId = 1;
    $articleId = 1;
    Article::withDescription($languageId)->findOrFail($articleId);
